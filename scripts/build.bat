@echo off

xcopy /Y "..\lib\SDL\lib\x64\SDL2.dll" "..\x64\Debug\SDL2.dll*"
xcopy /Y "..\lib\SDL\lib\x86\SDL2.dll" "..\Debug\SDL2.dll*"
xcopy /Y "..\lib\SDL\lib\x64\SDL2.dll" "..\x64\Release\SDL2.dll*"
xcopy /Y "..\lib\SDL\lib\x86\SDL2.dll" "..\Release\SDL2.dll*"

xcopy /Y "..\lib\SDL2_image\lib\x64\libpng16-16.dll" "..\x64\Debug\libpng16-16.dll*"
xcopy /Y "..\lib\SDL2_image\lib\x86\libpng16-16.dll" "..\Debug\libpng16-16.dll*"
xcopy /Y "..\lib\SDL2_image\lib\x64\libpng16-16.dll" "..\x64\Release\libpng16-16.dll*"
xcopy /Y "..\lib\SDL2_image\lib\x86\libpng16-16.dll" "..\Release\libpng16-16.dll*"

xcopy /Y "..\lib\SDL2_image\lib\x64\zlib1.dll" "..\x64\Debug\zlib1.dll*"
xcopy /Y "..\lib\SDL2_image\lib\x86\zlib1.dll" "..\Debug\zlib1.dll*"
xcopy /Y "..\lib\SDL2_image\lib\x64\zlib1.dll" "..\x64\Release\zlib1.dll*"
xcopy /Y "..\lib\SDL2_image\lib\x86\zlib1.dll" "..\Release\zlib1.dll*"

xcopy /Y "..\lib\SDL2_image\lib\x64\SDL2_image.dll" "..\x64\Debug\SDL2_image.dll*"
xcopy /Y "..\lib\SDL2_image\lib\x86\SDL2_image.dll" "..\Debug\SDL2_image.dll*"
xcopy /Y "..\lib\SDL2_image\lib\x64\SDL2_image.dll" "..\x64\Release\SDL2_image.dll*"
xcopy /Y "..\lib\SDL2_image\lib\x86\SDL2_image.dll" "..\Release\SDL2_image.dll*"

xcopy "..\GAP_275\assets" "..\Debug\assets" /s /e /i /y
xcopy "..\GAP_275\assets" "..\Release\assets" /s /e /i /y

exit
