#include "Image.h"

#include <SDL.h>
#include <SDL_image.h>

#include "Transform.h"
#include "Renderer.h"

Image::Image(const char* path)
	: m_pSurface(nullptr)
	, m_pTexture(nullptr)
	, m_pRect(nullptr)
{
	m_pSurface = IMG_Load(path);

	m_pTexture = SDL_CreateTextureFromSurface(Renderer::Get(), m_pSurface);

	m_pRect = new SDL_Rect();
}

Image::~Image()
{
	SDL_FreeSurface(m_pSurface);
	SDL_DestroyTexture(m_pTexture);
	delete m_pRect;
}

void Image::Update()
{
	// empty..
}

void Image::Render()
{
	SDL_Rect rect;

	rect.x = pTransform->position.x;
	rect.y = pTransform->position.y;
	rect.w = m_pRect->w * pTransform->scale.x;
	rect.h = m_pRect->h * pTransform->scale.y;

	SDL_RenderCopy(Renderer::Get(), m_pTexture, NULL, &rect);
}

void Image::SetSize(int w, int h)
{
	m_pRect->w = w;
	m_pRect->h = h;
}

void Image::SetNativeSize()
{
	SetSize(m_pSurface->w, m_pSurface->h);
}

SDL_Rect& Image::GetRect()
{
	return *m_pRect;
}
