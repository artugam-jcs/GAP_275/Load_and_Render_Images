#include "Player.h"

#include <iostream>
#include <SDL.h>

#include "Input.h"
#include "Color.h"
#include "Time.h"
#include "Image.h"
#include "GameObject.h"

#include "Transform.h"

Player::Player()
{
	m_color.r = 0;
	m_color.g = 255;
	m_color.b = 0;
}

Player::~Player()
{

}

void Player::Update()
{
	auto input = Input::GetInstance();

#if _DEBUG
	if (input->GetMouseDown())
	{
		auto pImage = GetComponent(typeid(Image).name());

		std::cout << pImage << std::endl;

		pImage->pTransform->scale.x += 5;
	}
#endif

	if (input->GetKey(SDLK_d))
		m_velocity.x = kSpeed;
	else if (input->GetKey(SDLK_a))
		m_velocity.x = -kSpeed;
	else
		m_velocity.x = 0;

	if (input->GetKey(SDLK_w))
		m_velocity.y = -kSpeed;
	else if (input->GetKey(SDLK_s))
		m_velocity.y = kSpeed;
	else
		m_velocity.y = 0;

	pTransform->position.x += m_velocity.x * (float)Time::DeltaTime();
	pTransform->position.y += m_velocity.y * (float)Time::DeltaTime();
}


void Player::Render()
{
	/*SDL_SetRenderDrawColor(pRenderer, m_color.r, m_color.g, m_color.b, SDL_ALPHA_OPAQUE);
	SDL_RenderFillRect(pRenderer, m_pRect);*/

	
}