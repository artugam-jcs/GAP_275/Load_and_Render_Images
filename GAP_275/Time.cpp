#include "Time.h"

Time* Time::s_pInstance = nullptr;
double Time::s_deltaTime = 0;

Time* Time::GetInstance()
{
	if (s_pInstance == nullptr)
		s_pInstance = new Time();
	return s_pInstance;
}

Time::Time()
	: m_frameTime(0)
	, m_lastFrameTime()
{
	m_lastFrameTime = std::chrono::high_resolution_clock::now();
}

void Time::Run()
{
	auto thisFrameTime = std::chrono::high_resolution_clock::now();
	std::chrono::duration<double> lastFrameDuration = thisFrameTime - m_lastFrameTime;
	s_deltaTime = lastFrameDuration.count();
	m_lastFrameTime = thisFrameTime;
}

double Time::DeltaTime()
{
	return s_deltaTime;
}
