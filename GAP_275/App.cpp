#include <iostream>
#include <chrono>

#include <SDL.h>

#include "App.h"
#include "GameObject.h"
#include "Component.h"
#include "Player.h"
#include "Image.h"
#include "Util.h"
#include "Time.h"
#include "Input.h"
#include "Renderer.h"

#define SCREEN_WIDTH 1280
#define SCREEN_HEIGHT 720

static int Random(int min, int max)
{
	return rand() % max + min;
}

App::App()
	: m_quit(false)
	, m_pWindow(nullptr)
	, m_gameObjects(NULL)
{

}

App::~App()
{
	Delete(m_gameObjects);

	delete Input::GetInstance();
	delete Time::GetInstance();

	SDL_Quit();
}

int App::Init()
{
	if (SDL_Init(SDL_INIT_VIDEO) != 0)
	{
		const char* error = SDL_GetError();
		std::cout << "Failed to initialize SDL." << std::endl;
		std::cout << error << std::endl;
		return 1;
	}

	std::cout << "Successfully initialized SDL!" << std::endl;

	m_pWindow = SDL_CreateWindow("Randomized_Mosaic",
		SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
		SCREEN_WIDTH, SCREEN_HEIGHT,
		SDL_WINDOW_SHOWN);

	if (m_pWindow == nullptr)
	{
		std::cout << "Failed to create window. Error: " << SDL_GetError();
		return 2;
	}

	// Create Renderer.
	SDL_Renderer* pRenderer = SDL_CreateRenderer(m_pWindow, -1, 0);

	if (pRenderer == nullptr)
	{
		std::cout << "Failed to create renderer. Error: " << SDL_GetError();
		SDL_DestroyWindow(m_pWindow);
		return 3;
	}

	Renderer::Set(pRenderer);

	srand((unsigned int) time(NULL));

	GameInit();

	return 0;
}

void App::AddToScene(GameObject* pGameObject)
{
	m_gameObjects.emplace_back(pGameObject);
}

void App::AddPlayer()
{
	auto pGO = new GameObject();
	{
		auto pPlayer = new Player();
		pGO->AddComponent(pPlayer);
	}
	
	{
		auto pImage = new Image("assets/yellowbird-downflap.png");
		pImage->SetNativeSize();
		pGO->AddComponent(pImage);
	}

	AddToScene(pGO);
}

void App::AddBackground()
{
	auto pGOBG1 = new GameObject();
	{
		auto pImage = new Image("assets/background-day.png");
		pImage->SetNativeSize();
		pGOBG1->AddComponent(pImage);
		AddToScene(pGOBG1);
	}

	auto pGOBG2 = new GameObject();
	{
		auto pImage = new Image("assets/background-day.png");
		pImage->SetNativeSize();
		pGOBG2->AddComponent(pImage);

		auto pRect = pImage->GetRect();
		pGOBG2->transform.position.x += pRect.w;
		AddToScene(pGOBG2);
	}
}

void App::AddRandomObjects()
{
	auto pRandom1 = new GameObject();
	{
		auto pImage = new Image("assets/BMP_example.bmp");
		pRandom1->AddComponent(pImage);

		pImage->SetSize(100, 100);

		auto pRect = pImage->GetRect();

		pRandom1->transform.position.x = (SCREEN_WIDTH - pRect.w) / 2.0f;
		pRandom1->transform.position.y = (SCREEN_HEIGHT - pRect.h) / 2.0f;
		
		AddToScene(pRandom1);
	}

	auto pRandom2 = new GameObject();
	{
		auto pImage = new Image("assets/GIF_example.gif");
		pRandom2->AddComponent(pImage);

		pImage->SetSize(200, 100);

		auto pRect = pImage->GetRect();

		pRandom2->transform.position.x = 1000;
		pRandom2->transform.position.y = 500;

		AddToScene(pRandom2);
	}

	auto pRandom3 = new GameObject();
	{
		auto pImage = new Image("assets/jcs090218.jpg");
		pRandom3->AddComponent(pImage);

		pImage->SetSize(100, 200);

		auto pRect = pImage->GetRect();

		pRandom3->transform.position.x = 100;
		pRandom3->transform.position.y = 200;

		AddToScene(pRandom3);
	}
}

void App::GameInit()
{
	AddBackground();
	AddRandomObjects();
	AddPlayer();
}

void App::Run()
{
	auto lastFrameTime = std::chrono::high_resolution_clock::now();

	while (!this->m_quit)
	{
		Time::GetInstance()->Run();

		auto input = Input::GetInstance();

		m_quit = !input->ProcessEvents();

		Update();
		Draw();
	}
}

void App::Update()
{
	/* Update logic! */
	for (int index = 0; index < m_gameObjects.size(); ++index)
	{
		auto pGameObject = m_gameObjects.at(index);
		pGameObject->Update();
	}
}

void App::Draw()
{
	auto pRenderer = Renderer::Get();

	SDL_SetRenderDrawColor(pRenderer, m_bgColor.r, m_bgColor.g, m_bgColor.b, SDL_ALPHA_OPAQUE);

	// XXX: Should this be here?
	SDL_SetRenderDrawBlendMode(pRenderer, SDL_BLENDMODE_BLEND);

	SDL_RenderClear(pRenderer);

	/* Draw logic! */
	for (int index = 0; index < m_gameObjects.size(); ++index)
	{
		auto pGameObject = m_gameObjects.at(index);
		pGameObject->Render();
	}

	SDL_RenderPresent(pRenderer);
}
