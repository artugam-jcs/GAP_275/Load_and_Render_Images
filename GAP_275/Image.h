#pragma once

struct SDL_Surface;
struct SDL_Texture;
struct SDL_Rect;

#include "Component.h"

class Image : public Component
{
private:
	SDL_Surface* m_pSurface;
	SDL_Texture* m_pTexture;

	SDL_Rect* m_pRect;

public:
	Image(const char*);
	~Image();

	virtual void Update() override;
	virtual void Render() override;
	void SetSize(int w, int h);
	void SetNativeSize();
	SDL_Rect& GetRect();
};

