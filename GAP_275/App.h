#pragma once

#include <vector>

// XXX: Include for class Color; use forward declaration after extraction.
#include "Color.h"

struct SDL_Window;
class GameObject;

class App
{
private:
	bool m_quit;

	SDL_Window* m_pWindow;

	std::vector<GameObject*> m_gameObjects;

	Color m_bgColor;  // Default is black.

public:
	App();
	~App();

	int Init();
	void Run();
	void Update();
	void Draw();

	// XXX: Ready for scene object.
	void AddToScene(GameObject*);

private:
	void GameInit();
	void AddPlayer();
	void AddBackground();
	void AddRandomObjects();
};

