#pragma once

#include <unordered_map>
#include <SDL_keyboard.h>

class Input
{
private:
	static Input* s_pInstance;

	std::unordered_map<char, bool> m_down;
	std::unordered_map<char, bool> m_up;
	std::unordered_map<char, bool> m_pressed;

	bool m_mouseDown;
	bool m_mouseUp;
	bool m_mousePressed;

public:
	static Input* GetInstance() 
	{
		if (s_pInstance == nullptr)
			s_pInstance = new Input();
		return s_pInstance;
	}

	bool GetKey(SDL_KeyCode code);
	bool GetKeyUp(SDL_KeyCode code);
	bool GetKeyDown(SDL_KeyCode code);

	bool GetMouse();
	bool GetMouseUp();
	bool GetMouseDown();

	void ClearInputBuffer();
	bool ProcessEvents();

private:
	Input();
};

