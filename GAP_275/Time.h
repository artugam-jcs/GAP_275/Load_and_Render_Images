#pragma once

#include <chrono>

class Time
{
private:
	static Time* s_pInstance;
	static double s_deltaTime;

	double m_frameTime;
	std::chrono::steady_clock::time_point m_lastFrameTime;

public:
	static Time* GetInstance();
	
	void Run();
	static double DeltaTime();
private:
	Time();
};

