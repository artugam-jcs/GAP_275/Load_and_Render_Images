#pragma once

#include <vector>
#include <typeinfo>

#include "Transform.h"

class Component;

class GameObject
{
private:
	std::vector<Component*> m_components;

public:
	Transform transform;

	~GameObject();

	void Update();
	void Render();

	void AddComponent(Component* component);

	template<typename T>
	Component* GetComponent(T type);
};

template<typename T>
Component* GameObject::GetComponent(T type)
{
	for (int index = 0; index < m_components.size(); ++index)
	{
		auto pComponent = m_components.at(index);

		if ((T)pComponent != nullptr)
			return pComponent;
	}

	return nullptr;
}
