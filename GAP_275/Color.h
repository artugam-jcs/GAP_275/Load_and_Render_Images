#pragma once

#include <SDL.h>

class Color
{
public:
	Uint8 r;
	Uint8 g;
	Uint8 b;

	Color()
		: Color(0, 0, 0)
	{ }

	Color(Uint8 r, Uint8 g, Uint8 b)
		: r(r)
		, g(g)
		, b(b)
	{ }
};

