#include "GameObject.h"

#include "Util.h"
#include "Component.h"


GameObject::~GameObject()
{
	Delete(m_components);
}

void GameObject::Update()
{
	for (int index = 0; index < m_components.size(); ++index)
	{
		auto pComponent = m_components.at(index);
		pComponent->Update();
	}
}

void GameObject::Render()
{
	for (int index = 0; index < m_components.size(); ++index)
	{
		auto pComponent = m_components.at(index);
		pComponent->Render();
	}
}

void GameObject::AddComponent(Component* pComponent)
{
	pComponent->pGameObject = this;
	pComponent->pTransform = &this->transform;

	this->m_components.emplace_back(pComponent);
}
