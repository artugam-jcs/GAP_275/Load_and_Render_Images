#pragma once

#include "GameObject.h"

class Transform;

class Component
{
public:
	Transform* pTransform;
	GameObject* pGameObject;

	virtual void Update() = 0;
	virtual void Render() = 0;

	void AddComponent(Component* pComponent);

	template<typename T>
	Component* GetComponent(T type);
};

template<typename T>
Component* Component::GetComponent(T type)
{
	return pGameObject->GetComponent(type);
}
