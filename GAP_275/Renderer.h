#pragma once

#include <SDL.h>

class Renderer
{
public:
	static SDL_Renderer* s_pRenderer;

public:
	static void Set(SDL_Renderer* pRenderer)
	{
		s_pRenderer = pRenderer;
	}
	static SDL_Renderer* Get()
	{
		return s_pRenderer;
	}
};

