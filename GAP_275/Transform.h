#pragma once

#include "Vector2.h"

class Transform
{
public:
	Vector2 position;
	Vector2 scale;

	Transform()
		: position(0, 0)
		, scale(1, 1)
	{ }
};

