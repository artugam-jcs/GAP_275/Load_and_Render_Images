#pragma once

#include "Component.h"
#include "Color.h"
#include "Vector2.h"

struct SDL_Rect;
struct SDL_Renderer;

class Player : public Component
{
private:
	static constexpr float kSpeed = 500.0f;

	Color m_color;

	Vector2 m_velocity;

public:
	Player();
	~Player();

	virtual void Update() override;
	virtual void Render() override;
};

