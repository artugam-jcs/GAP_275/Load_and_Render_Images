#include "Input.h"

#include <iostream>
#include <SDL.h>

Input* Input::s_pInstance = nullptr;

Input::Input()
	: m_down(NULL)
	, m_up(NULL)
	, m_pressed(NULL)
	, m_mouseDown(false)
	, m_mouseUp(false)
	, m_mousePressed(false)
{

}

bool Input::GetKey(SDL_KeyCode code)
{
	return m_pressed[code];
}

bool Input::GetKeyUp(SDL_KeyCode code)
{
	return m_up[code];
}

bool Input::GetKeyDown(SDL_KeyCode code)
{
	return m_down[code];
}

bool Input::GetMouse()
{
	return m_mousePressed;
}

bool Input::GetMouseUp()
{
	return m_mouseUp;
}

bool Input::GetMouseDown()
{
	return m_mouseDown;
}

void Input::ClearInputBuffer()
{
	m_mouseDown = false;
	m_mouseUp = false;

	m_down.clear();
	m_up.clear();
}

bool Input::ProcessEvents()
{
	ClearInputBuffer();

	SDL_Event evt;

	while (SDL_PollEvent(&evt) != 0)
	{
		char key = evt.key.keysym.sym;

		switch (evt.type)
		{
		case SDL_MOUSEBUTTONUP:
		{
			if (m_mousePressed)
				m_mouseUp = true;
			else
				m_mouseUp = false;

			m_mousePressed = false;
		}
		break;
		case SDL_MOUSEBUTTONDOWN:
		{
			if (!m_mousePressed)
				m_mouseDown = true;
			else
				m_mouseDown = false;

			m_mousePressed = true;
		}
		break;
		case SDL_KEYUP:
		{
			if (m_pressed[key])
				m_up[key] = true;
			else
				m_up[key] = false;

			m_pressed[key] = false;
		}
		break;
		case SDL_KEYDOWN:
		{
			if (!m_pressed[key])
				m_down[key] = true;
			else
				m_down[key] =false;

			m_pressed[key] = true;
		}
		break;

		case SDL_WINDOWEVENT:
		{
			if (evt.window.event == SDL_WINDOWEVENT_CLOSE)
				return false;
		}
		break;
		}
	}
	return true;
}
